import '../styles/globals.css'
import '../styles/language.css'

import { ThemeProvider } from '@material-ui/core/styles'
import theme from '../theme/theme'
import NProgress from 'nprogress' //nprogress module
import 'nprogress/nprogress.css'
import 'react-datetime/css/react-datetime.css'
import 'react-google-flight-datepicker/dist/main.css'

import Header from '../components/header/header'
import { Router } from 'next/router'
import { appWithTranslation } from '../i18n'

Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

function MyApp({ Component, pageProps }) {
  NProgress.configure({ showSpinner: false })
  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default appWithTranslation(MyApp)
