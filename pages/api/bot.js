require('dotenv').config()

// replace the value below with the Telegram token you receive from @BotFather
const token = process.env.TELEGRAM_TOKEN

const TelegramBot = require('node-telegram-bot-api')

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, { polling: true })

export default (req, res) => {
  const {
    email,
    tour_type,
    addresses,
    passengers,
    car_class,
    comment,
  } = req.body

  let text = ''

  for (let i = 0; i < addresses.length; i++) {
    text += `
    Маршрут: ${addresses[i] && addresses[i].from_place} - ${
      addresses[i] && addresses[i].to_place
    }
    Время путешествия: ${addresses[i] && addresses[i].start_date} - ${
      addresses[i] && addresses[i].end_date
    }
    ---------------------------

    `
  }

  const mailOption = `

Email: ${email}
Пассажиры: ${passengers}
Тип тура: ${tour_type['ru']}
Класс машины: ${car_class.class}/${car_class.price_from}
Kommeнт: ${comment}

${text}

    `

  bot.sendMessage(`@goluxtrip`, mailOption)
}
