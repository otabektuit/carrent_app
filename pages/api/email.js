require('dotenv').config()
const nodemailer = require('nodemailer')

export default async (req, res) => {
  const { email, tour_type, addresses, passengers, car_class } = req.body

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.googlemail.com', // Gmail Host
    port: 465, // Port
    secure: true, // this is true as port is 465
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD,
    },
  })

  let text = ''

  for (let i = 0; i < addresses.length; i++) {
    text += `
    <ul>
      <li>
      ${addresses[i] && addresses[i].from_place} - ${
      addresses[i] && addresses[i].to_place
    }
      </li>
      <li>
      ${addresses[i] && addresses[i].start_date} - ${
      addresses[i] && addresses[i].end_date
    }
      </li>
    </ul>
    
    `
  }

  const mailOption = {
    from: process.env.EMAIL,
    to: `zayniddin4346@gmail.com`,
    subject: `Order from GLT by Obdev`,
    html: `
      <div style="width:100%;display:block;">
        <h3>Client Email: ${email}</h3>
        <h4>Passengers: ${passengers}</h4>

        <p>Tour Type: ${tour_type}</p>
        <p>Car Class: ${car_class.en}/${car_class.price}</p>

        ${text}

      </div>
    `,
  }
  transporter.sendMail(mailOption, (err, data) => {
    if (err) {
      console.log(err)
      res.send('error' + JSON.stringify(err))
    } else {
      res.send('success')
    }
  })
}
