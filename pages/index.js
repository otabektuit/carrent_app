import axios from 'axios'
import Head from 'next/head'
import { useState } from 'react'
import Aboutus from '../components/aboutus'
import Advantages from '../components/advantages'
import CallUs from '../components/CallUs/CallUs'
import Cars from '../components/cars/Cars'
import Footer from '../components/footer/footer'
import Hero from '../components/hero/hero'
import PopularLocationList from '../components/popular_location_list'
import SEO from '../components/seo'
import { i18n } from '../i18n'

export default function Home(props) {
  const { regions, cars, categories, models } = props
  const [meta] = useState({
    title: {
      ru: 'Goluxtrip (GLT) - услуги шофера',
      en: 'Goluxtrip (GLT) - chauffer services',
    },
    description: {
      ru:
        'Служба шоферов GoLuxTravel (GLT) была основана Зайном Умаровым в 2020 году. Работая с международными экспертами, которые приезжают для выполнения своей миссии в Узбекистан, он увидел необходимость в единой профессиональной службе шофера. Видение Зейна состоит в том, чтобы упростить жизнь клиентов и упростить им жизнь, а также сделать более разумным бронирование транспортных услуг для индивидуальных или групповых путешественников. GLT предоставляет надежных и опытных водителей, которые досконально знают все города Узбекистана. Мы понимаем потребность в комфорте или роскоши после долгого рабочего дня в офисе, в перелете и в дальнем пункте назначения, поэтому мы готовы обеспечить вежливый и быстрый доступ к вашему конечному пункту назначения.',
      en: `GoLuxTrip (GLT) chauffer services was founded by Zayn Umarov in 2020. Working with international experts who visit to undertake their mission in Uzbekistan, he saw a need for a single professional chauffeur service. Zayn’s vision is to make customers life easier and less hustle & smarter way to book transportation services for single or group travelers. GLT provides reliable and well experienced chauffeurs who have in-depth knowledge of all-around the city locations of Uzbekistan. We understand the need for comfort or luxury after a long day/hours at office, flight & far destination thus, we are prepared to provide courteous and swift drive to your final destination`,
    },
    keywords: {
      ru: `Услуги водителя, автомобиль, транспорт, транспорт, трансферы, встреча в аэропорту, свадьба, Mercedes-Benz, транспортные услуги, поездки на объект, шоферы, водители, поездка, автомобиль, аренда автомобиля, фургоны, групповая поездка, тур, частный водитель, Вито, Виано, KIA, GLT, аренда авто, аренда водителя.`,
      en: `Chauffeur services, car, transportation, transport, transfers, airport pick-ups, wedding, Mercedes-Benz, transport services, project site trips, chauffeurs, drivers, ride, vehicle, rent car, vans, group trip, tour, private driver, Vito, Viano, KIA, GLT, hire a car, hire a driver. `,
    },
  })
  return (
    <>
      <SEO
      // title={meta.title[i18n.language]}
      // description={meta.description[i18n.language]}
      // keywords={meta.keywords[i18n.language]}
      // image={`/images/glt.png`}
      />
      {categories ? <Hero categories={categories} /> : ''}
      {categories ? (
        <Cars
          // cars={item.cars}
          // key={i}
          models={models}
          categories={categories.sort((a, b) =>
            a.id < b.id ? 1 : b.id < a.id ? -1 : 0
          )}
        />
      ) : (
        ''
      )}
      {/* {cars ? <Cars cars={cars} /> : ''} */}
      {regions ? <PopularLocationList regions={regions} /> : ''}
      <Aboutus />
      <Advantages />
      <Footer />
      <CallUs />
    </>
  )
}

export async function getServerSideProps() {
  const categories = await axios.get(process.env.CATEGORY_API).catch((err) => {
    console.log(err)
  })

  const regions = await axios.get(process.env.REGION_API).catch((err) => {
    console.log(err)
  })

  const cars = await axios.get(process.env.CAR_API).catch((err) => {
    console.log(err)
  })

  const models = await axios.get(process.env.MODEL_API).catch((err) => {
    console.log(err)
  })

  return {
    props: {
      regions: regions ? regions.data : null,
      cars: cars ? cars.data : null,
      categories: categories ? categories.data : null,
      models: models ? models.data : null,
    },
  }
}
