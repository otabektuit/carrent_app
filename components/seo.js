import Head from 'next/head'
import { withTranslation } from '../i18n'

function SEO({ t, title, description, image, keywords }) {
  return (
    <Head lang={'en'}>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />

      {/* <title>{title || `Goluxtrip`}</title> */}
      <title>{`Goluxtrip (GLT) - chauffer services`}</title>
      <meta
        name="description"
        content={`Airport pick-up/transfer , Home – Office – Home service , Project site visits all around the Republic , Child service (school – home - school) , Tours , Wedding , Chauffeur services, car, transportation, transport, transfers, airport pick-ups, wedding, Mercedes-Benz, transport services, project site trips, chauffeurs, drivers, ride, vehicle, rent car, vans, group trip, tour, private driver, Vito, Viano, KIA, GLT, hire a car, hire a driver. `}
      />
      <meta
        name="keywords"
        content={`Chauffeur services, car, transportation, transport, transfers, airport pick-ups, wedding, Mercedes-Benz, transport services, project site trips, chauffeurs, drivers, ride, vehicle, rent car, vans, group trip, tour, private driver, Vito, Viano, KIA, GLT, hire a car, hire a driver. `}
      />

      <meta property="og:type" content="website" />
      <meta
        property="og:title"
        content={`Goluxtrip (GLT) - chauffer services`}
        key="ogtitle"
      />
      <meta
        property="og:description"
        content={`Airport pick-up/transfer , Home – Office – Home service , Project site visits all around the Republic , Child service (school – home - school) , Tours , Wedding , Chauffeur services, car, transportation, transport, transfers, airport pick-ups, wedding, Mercedes-Benz, transport services, project site trips, chauffeurs, drivers, ride, vehicle, rent car, vans, group trip, tour, private driver, Vito, Viano, KIA, GLT, hire a car, hire a driver. `}
        key="ogdesc"
      />
      <meta
        property="og:site_name"
        content={`Goluxtrip (GLT) - chauffer services`}
        key="ogsitename"
      />
      {/* <meta property="og:url" content="https://goodzone.uz" key="ogurl" /> */}
      <meta
        property="og:image"
        content={
          image
            ? image
            : 'https://cdn.delever.uz/goodzone/3af8b7fb-f64a-4680-913e-f6c5187e3d6e'
        }
        key="ogimage"
      />

      <meta name="twitter:card" content="summary" />
      <meta
        name="twitter:title"
        content={`Goluxtrip (GLT) - chauffer services`}
      />
      <meta
        name="twitter:description"
        content={`Airport pick-up/transfer , Home – Office – Home service , Project site visits all around the Republic , Child service (school – home - school) , Tours , Wedding , Chauffeur services, car, transportation, transport, transfers, airport pick-ups, wedding, Mercedes-Benz, transport services, project site trips, chauffeurs, drivers, ride, vehicle, rent car, vans, group trip, tour, private driver, Vito, Viano, KIA, GLT, hire a car, hire a driver. `}
      />
      <meta name="twitter:site" content={`GoLuxTrip`} />
      <meta name="twitter:creator" content="Obdev" />
      <meta
        name="twitter:image"
        content={
          image
            ? image
            : 'https://cdn.delever.uz/goodzone/3af8b7fb-f64a-4680-913e-f6c5187e3d6e'
        }
      />

      {/* <link rel="canonical" href="https://goodzone.uz/" /> */}
      <link rel="icon" href="/images/glt.png" />
      <link
        href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400;1,700&family=Poppins:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet"
      />
    </Head>
  )
}

export default withTranslation('common')(SEO)
