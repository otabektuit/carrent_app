import React, { useEffect, useRef, useState } from 'react'
import { i18n } from '../i18n'

const LanguagePicker = () => {
  const langs = [
    {
      name: 'ru',
      title: 'Русский',
      image: '/images/russia.svg',
      abr: 'РУ',
    },
    {
      name: 'en',
      title: 'English',
      image: '/images/britain.png',
      abr: 'EN',
    },
  ]

  const [open, setOpen] = useState(false)

  const [selected, setSelected] = useState(
    i18n.language === 'ru' ? langs[0] : langs[1]
  )

  const handleChange = (index) => (event) => {
    let lang = langs[index]
    i18n.changeLanguage(lang.name)
    setSelected(lang)
    setOpen(false)
  }
  const wrapperRef = useRef(null)

  return (
    <div className={`selected_lang`}>
      <div
        className={`d-flex align-items-center selected_item`}
        style={{
          cursor: 'pointer',
        }}
        onClick={() => {
          setOpen(!open)
        }}
      >
        <img src={selected.image} width={'30px'} alt={`Goluxtrip`} />
        <span className={`mz-2`}>{selected.title}</span>
      </div>
      {open ? (
        <ul className={`language_list shadow`} ref={wrapperRef}>
          {langs.map((lang, i) => (
            <li className={`language_item`} onClick={handleChange(i)} key={i}>
              <img src={lang.image} width={`30px`} alt={`Goluxtrip`} />

              <span>{lang.title}</span>
            </li>
          ))}
        </ul>
      ) : (
        ''
      )}
    </div>
  )
}

export default LanguagePicker
