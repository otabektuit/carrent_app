import { Container, Grid } from '@material-ui/core'
import React, { useState } from 'react'
import { i18n } from '../../i18n'
import styles from './advantages.module.scss'
export default function Advantages() {
  const [services, setServices] = useState([
    {
      en: 'Airport pick-up/transfer',
      ru: 'Встреча в аэропорту / трансфер',
      img: '/images/airport.svg',
    },
    {
      en: 'Home – Office – Home service',
      ru: 'Дом - Офис - Домашний сервис',
      img: '/images/work.svg',
    },
    {
      en: 'Project site visits all around the Republic',
      ru: 'Посещение объектов проекта по всей республике',
      img: '/images/uzbekistan.svg',
    },
    {
      en: 'Child service (school – home - school)',
      ru: 'Обслуживание детей (школа - дом - школа)',
      img: '/images/adoption.svg',
    },
    {
      en: 'Tours',
      ru: 'Туры',
      img: '/images/destination.svg',
    },

    {
      en: 'Wedding',
      ru: 'Свадьбы',
      img: '/images/location1.svg',
    },
  ])
  return (
    <div className={styles.wrapper} id={`services`}>
      <Container>
        <div className={styles.block}>
          <h1 className="title">
            {i18n.language === 'en' ? 'Our services' : 'Наши сервисы'}
          </h1>
          <Grid container spacing={3}>
            {services.map((item, i) => (
              <Grid item lg={4} md={6} xs={12} key={i}>
                <div className={styles.card}>
                  <div className={styles.card_icon}>
                    <img src={item.img} alt="GLT image" />
                  </div>
                  <div className={styles.card_body}>
                    <h6>{item[i18n.language]}</h6>
                    {/* <p>Call us Anywhere Anytime</p> */}
                  </div>
                </div>
              </Grid>
            ))}
          </Grid>
        </div>
      </Container>
    </div>
  )
}
