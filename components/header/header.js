import { Container, Grid } from '@material-ui/core'
import React from 'react'
import { i18n } from '../../i18n'
import LanguagePicker from '../LanguagePicker'
import cls from './header.module.scss'

function Header() {
  return (
    <header className={cls.container}>
      <Container>
        <Grid container>
          <div className={cls.wrapper}>
            <a href={`#`}>
              <img src={`/images/glt1.png`} alt={`Goluxtrip`} width={160} />
            </a>
            <ul className={cls.list}>
              <li className={cls.list_item}>
                <a href="#cars" className={cls.contact_tel}>
                  {i18n.language === 'en' ? 'Vehicles' : 'Машины'}
                </a>
              </li>
              <li className={cls.list_item}>
                <a href="#about" className={cls.contact_tel}>
                  {i18n.language === 'en' ? 'About us' : 'Про нас'}
                </a>
              </li>
              <li className={cls.list_item}>
                <a href="#services" className={cls.contact_tel}>
                  {i18n.language === 'en' ? 'Services' : 'Услуги'}
                </a>
              </li>

              {/* <li className={cls.list_item}>
                <a href="tel: +998946264346" className={cls.contact_tel}>
                  +998 94 626 43 46
                </a>
              </li> */}
              <li className={cls.list_item}>
                <LanguagePicker />
              </li>
            </ul>
          </div>
        </Grid>
      </Container>
    </header>
  )
}

export default Header
