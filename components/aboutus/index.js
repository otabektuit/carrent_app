import { Container, Grid } from '@material-ui/core'
import React, { useState } from 'react'
import { i18n } from '../../i18n'
import styles from './aboutus.module.scss'
export default function Aboutus() {
  const about = {
    ru: `
    <b> GoLuxTravel (GLT) </b> chauffer services была основана <b> Зайн Умаров </b> в 2020 году. Работа с международными экспертами.
                приезжающих с миссией в Узбекистан, он увидел
                потребность в услугах единственного профессионального шофера. Видение Зейна
                сделать жизнь клиентов проще, менее суетливее и умнее
                заказать транспортные услуги для индивидуальных или групповых путешественников.
                GLT предоставляет надежных и опытных водителей, у которых есть
                глубокое знание городских локаций
                Узбекистан. Мы понимаем необходимость комфорта или роскоши после
                долгий день / часы в офисе, в полете и в дальнем пункте назначения, поэтому мы
                готовы обеспечить вежливую и быструю поездку на финальный
                пункт назначения.
    `,
    en: `
    <b>GoLuxTravel (GLT)</b> chauffer services was founded by <b>Zayn Umarov</b> in 2020. Working with international experts
                who visit to undertake their mission in Uzbekistan, he saw a
                need for a single professional chauffeur service. Zayn’s vision
                is to make customers life easier and less hustle & smarter way
                to book transportation services for single or group travelers.
                GLT provides reliable and well experienced chauffeurs who have
                in-depth knowledge of all-around the city locations of
                Uzbekistan. We understand the need for comfort or luxury after a
                long day/hours at office, flight & far destination thus, we are
                prepared to provide courteous and swift drive to your final
                destination.
                `,
  }
  return (
    <div className={styles.wrapper} id={`about`}>
      <Container>
        <div className={styles.block}>
          <h1 className="title">
            {i18n.language === 'en' ? 'About us' : 'Про нас'}
          </h1>
          <Grid container spacing={3}>
            {/* <Grid item lg={6} md={12} xs={12}>
              <img
                src={`https://media-exp1.licdn.com/dms/image/C4E1BAQEt2sHvQDyupg/company-background_10000/0/1519799066087?e=2159024400&v=beta&t=pT6NocA-0o3Y2D1vQ7rfXXQ8VKXz1x5zKwngkKEz_Hw`}
                alt={`GLT image`}
                width="100%"
              />
            </Grid> */}
            <Grid item lg={12} md={12} xs={12}>
              <div className={styles.content}>
                {i18n.language === 'ru' ? (
                  <>
                    Служба шоферов <b>GoLuxTravel (GLT)</b> была основана{' '}
                    <b>Зайном Умаровым</b> в 2020 году. Работая с международными
                    экспертами, которые приезжают для выполнения своей миссии в
                    Узбекистан, он увидел необходимость в единой
                    профессиональной службе шофера. Видение Зейна состоит в том,
                    чтобы упростить жизнь клиентов и упростить им жизнь, а также
                    сделать более разумным бронирование транспортных услуг для
                    индивидуальных или групповых путешественников. GLT
                    предоставляет надежных и опытных водителей, которые
                    досконально знают все города Узбекистана. Мы понимаем
                    потребность в комфорте или роскоши после долгого рабочего
                    дня в офисе, в перелете и в дальнем пункте назначения,
                    поэтому мы готовы обеспечить вежливый и быстрый доступ к
                    вашему конечному пункту назначения.
                  </>
                ) : (
                  <>
                    {' '}
                    <b>GoLuxTravel (GLT)</b> chauffer services was founded by{' '}
                    <b>Zayn Umarov</b> in 2020. Working with international
                    experts who visit to undertake their mission in Uzbekistan,
                    he saw a need for a single professional chauffeur service.
                    Zayn’s vision is to make customers life easier and less
                    hustle & smarter way to book transportation services for
                    single or group travelers. GLT provides reliable and well
                    experienced chauffeurs who have in-depth knowledge of
                    all-around the city locations of Uzbekistan. We understand
                    the need for comfort or luxury after a long day/hours at
                    office, flight & far destination thus, we are prepared to
                    provide courteous and swift drive to your final destination.
                  </>
                )}
              </div>
            </Grid>
          </Grid>
        </div>
      </Container>
    </div>
  )
}
