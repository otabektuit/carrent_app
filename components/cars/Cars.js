import { Container, Grid } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { i18n } from '../../i18n'
import cls from './Cars.module.scss'

const CarCard = (props) => {
  const {
    src = 'https://www.kayak.com/h/run/api/image?caller=Cars&height=196&crop=true&url=/carimages/generic/02_economy_white.png',
    title = 'Renault Symbol',
    category = 'Economy',
    place = '4',
    type = 'Automatic',
    distance_covered = '1200',
    temperature = 'A/C',
    descrption = '',
    brand = 'https://content.r9cdn.net/rimg/provider-logos/cars/h/sixtrentacar.png?crop=false&width=120&height=60&fallback=default3.png&_v=0b58f221465e1724ea3b436b16732e12bb3c5879',
  } = props
  return (
    <div className={cls.card}>
      <div className={cls.header}>
        <img src={src} alt={'Car '} />
      </div>
      <div className={cls.body}>
        <div className={cls.titles}>
          <div className={cls.name}>
            <h2>{title}</h2>
            <p>{category}</p>
          </div>
          <div className={cls.brand}>
            <img src={brand} alt="brand" />
          </div>
        </div>
        {/* <div className={cls.details}>
          <div className={cls.detail}>
            <img src={'/images/alloy-wheel.svg'} alt={'Carrent'} width={30} />
            <span>{distance_covered} kms</span>
          </div>
          <div className={cls.detail}>
            <img src={'/images/gearshift.svg'} alt={'Carrent'} width={30} />
            <span>{type}</span>
          </div>
          <div className={cls.detail}>
            <img src={'/images/engine.svg'} alt={'Carrent'} width={30} />
            <span>{temperature}</span>
          </div>
          <div className={cls.detail}>
            <img src={'/images/user.svg'} alt={'Carrent'} width={30} />
            <span>{place}</span>
          </div>
        </div> */}
      </div>
      <div className={cls.info}>
        <p>{descrption}</p>
      </div>
    </div>
  )
}

const Cars = (props) => {
  const { cars, models, categories } = props
  const [inCategories, setInCategories] = useState(categories || [])
  function compare(a, b) {
    if (a.id > b.id) {
      return -1
    }
    if (a.id < b.id) {
      return 1
    }
    return 0
  }

  return (
    <section className={cls.container} id={`cars`}>
      <Container>
        <Grid container spacing={4}>
          <Grid item lg={12}>
            <h1 className={'title'}>
              {i18n.language === 'en'
                ? 'Available vehicles'
                : 'Доступные автомобили'}
            </h1>
          </Grid>
          {categories
            ? inCategories.map((cat, c) =>
                cat.cars.map((item, i) => (
                  <Grid item lg={4} key={i}>
                    <CarCard
                      src={process.env.BASE_URL + item.image[0].url}
                      title={item[i18n.language]}
                      category={cat[i18n.language]}
                      brand={
                        process.env.BASE_URL +
                        (models
                          ? models.filter((mod, m) => mod.id === item.model)[0]
                              .image[0].url
                          : item.model.image[0].url)
                      }
                      descrption={
                        i18n.language == 'en'
                          ? item.description_en
                          : item.description_ru
                      }
                    />
                  </Grid>
                ))
              )
            : ''}
        </Grid>
      </Container>
    </section>
  )
}

export default Cars
