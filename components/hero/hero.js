import { Button, Container, Grid, TextField } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import cls from './hero.module.scss'
import PlaceOutlinedIcon from '@material-ui/icons/PlaceOutlined'
import GpsFixedOutlinedIcon from '@material-ui/icons/GpsFixedOutlined'
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined'
import MailOutlineOutlinedIcon from '@material-ui/icons/MailOutlineOutlined'
import SupervisorAccountOutlinedIcon from '@material-ui/icons/SupervisorAccountOutlined'
import IndeterminateCheckBoxOutlinedIcon from '@material-ui/icons/IndeterminateCheckBoxOutlined'
import { i18n } from '../../i18n'
import axios from 'axios'
import Success from '../success/success'
import MessageOutlinedIcon from '@material-ui/icons/MessageOutlined'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import CheckIcon from '@material-ui/icons/Check'
import Datetime from 'react-datetime'
import {
  SingleDatePicker,
  RangeDatePicker,
} from 'react-google-flight-datepicker'

function Input(props) {
  const {
    name,
    placeholder,
    type = 'text',
    id,
    value,
    htmlFor,
    onChange,
    children,
    isDate = false,
    regions,
    key,
    setData,
    data,
    index,
    startDate = new Date(),
    endDate = new Date(),
  } = props

  const [selectRegions, setSelectRegions] = useState([])

  const handleSelect = (idx) => (ev) => {
    if (idx >= 0) {
      let addresses = data.addresses
      addresses[index] = {
        ...addresses[index],
        [name]: regions[idx][i18n.language],
      }
      setData({
        ...data,
        addresses: addresses,
      })
    } else setData({ ...data, [ev.target.name]: ev.target.value })
    setShow(false)
  }

  const handleKeyUp = (e) => {
    let arr = []
    if (e.target.value.length > 0) {
      regions.forEach((element) => {
        if (
          element[i18n.language]
            .toLowerCase()
            .includes(e.target.value.toLowerCase())
        ) {
          arr.push(element)
        }
      })
      if (arr.length > 0) setShow(true)
      else setShow(false)
    } else setShow(false)
    setSelectRegions(arr)
  }

  const [show, setShow] = useState(false)

  return type === 'date' ? (
    <label htmlFor={id} className={cls.date}>
      <RangeDatePicker
        name={name}
        placeholder={placeholder}
        id={id}
        // type="timedate"

        startDate={startDate}
        endDate={endDate}
        onChange={(startDate, endDate) => {
          onChange(index, startDate, endDate)
        }}
        minDate={new Date(1900, 0, 1)}
        maxDate={new Date(2100, 0, 1)}
        dateFormat="DD-MM-YYYY"
        monthFormat="MMM YYYY"
        startDatePlaceholder="Start Date"
        endDatePlaceholder="End Date"
        disabled={false}
        className="my-own-class-name"
        startWeekDay="monday"
      />
    </label>
  ) : (
    <label htmlFor={id} className={cls.label}>
      <input
        name={name}
        placeholder={placeholder}
        id={id}
        onChange={onChange}
        value={value}
        className={cls.input}
        type={type}
        // type="timedate"
        onKeyUp={isDate ? () => {} : handleKeyUp}
      />

      {children}
      {show && !isDate ? (
        <div className={cls.region_list}>
          {selectRegions.map((item, i) => (
            <div
              key={i}
              className={cls.region_item}
              onClick={handleSelect(item.key)}
            >
              {item[i18n.language]}
            </div>
          ))}
        </div>
      ) : (
        ''
      )}
    </label>
  )
}

function Select(props) {
  const { lists, selected, data, setData, type = `` } = props

  const handleClick = (item) => (e) => {
    setData({ ...data, [type]: item })
    handleClose()
  }

  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleToggle = () => {
    setOpen(!open)
  }
  return (
    <div className={cls.select_wrapper}>
      <div className={cls.selected_item} onClick={handleToggle}>
        <p>{type === 'car_class' ? selected.class : selected[i18n.language]}</p>
        <ExpandMoreIcon />
      </div>
      {open ? (
        <ul className={cls.select_list}>
          {lists.map((item, i) => (
            <li className={cls.select_item} key={i} onClick={handleClick(item)}>
              <span>
                {type === 'tour_type' ? (
                  item[i18n.language] === selected[i18n.language] ? (
                    <CheckIcon />
                  ) : (
                    ''
                  )
                ) : type === 'car_class' ? (
                  item.class === selected.class ? (
                    <CheckIcon />
                  ) : (
                    ''
                  )
                ) : (
                  ''
                )}
              </span>
              <p>{type === 'car_class' ? item.class : item[i18n.language]}</p>
            </li>
          ))}
        </ul>
      ) : (
        ''
      )}
    </div>
  )
}

function Hero(props) {
  const { categories } = props

  const address = { from_place: '', to_place: '', start_date: '', end_date: '' }
  const [car_classes, setCarClasses] = useState([
    {
      class: 'Economy',
      price_from: '100$',
    },
    {
      class: 'Business',
      price_from: '250$',
    },
  ])
  const [tours, setTours] = useState([
    { en: 'One way', ru: 'В одну сторону' },
    { en: 'Round Trip', ru: 'Поездка в обе конца' },
    { en: 'Multi City Trip', ru: 'Поездка по нескольким городам' },
  ])
  const [data, setData] = useState({
    addresses: [{ from_place: '', to_place: '', start_date: '', end_date: '' }],
    car_class: car_classes[0],
    email: '',
    passengers: '',
    tour_type: tours[0],
    location: '',
    comment: '',
  })

  useEffect(() => {
    // getLocation()
    // console.log(data)
  }, [])

  function getLocation() {
    if (navigator.geolocation)
      navigator.geolocation.getCurrentPosition((position) =>
        showPosition(position)
      )
  }

  function showPosition(position) {
    setData({
      ...data,
      location: `lat = ${position.coords.latitude}/long = ${position.coords.longitude}`,
    })
  }

  const [selectedCarClass, setSelectedCarClass] = useState(car_classes[0])

  const [tour_type, setTourType] = useState(0)

  const [regions, setRegions] = useState([
    {
      ru: 'Ташкент(airport)',
      en: 'Tashkent (аэропорт)',
      key: 0,
    },
    {
      ru: 'Самарканд (аэропорт)',
      en: 'Samarkand (airport)',
      key: 1,
    },
    {
      ru: 'Буxaра (аэропорт)',
      en: 'Bukhara (airport)',
      key: 2,
    },
    {
      ru: 'Андижан (аэропорт)',
      en: 'Andijan (airport)',
      key: 3,
    },
    {
      ru: 'Фергана (аэропорт)',
      en: 'Fergana (airport)',
      key: 4,
    },

    {
      ru: 'Навои (аэропорт)',
      en: 'Navoi (airport)',
      key: 5,
    },
    {
      ru: 'Сурхандарья (аэропорт)',
      en: 'Surkhandaryo (airport)',
      key: 6,
    },

    {
      ru: 'Кашкадарья (аэропорт)',
      en: 'Qashaqadaryo (airport)',
      key: 7,
    },
    {
      ru: 'Хорезм (аэропорт)',
      en: 'Khorezm (airport)',
      key: 8,
    },
    {
      ru: 'Нукус (аэропорт)',
      en: 'Nukus (airport)',
      key: 9,
    },
    {
      ru: 'Термиз (аэропорт)',
      en: 'Termiz (airport)',
      key: 10,
    },
    {
      en: 'Tashkent South (station)',
      ru: 'Ташкент-Южный (станция)',
      key: 11,
    },
    {
      en: 'Tashkent Northern (station)',
      ru: 'Ташкент-Пассажирский (станция)',
      key: 12,
    },
    {
      en: 'Karshi (station)',
      ru: 'Карши (станция)',
      key: 13,
    },
    {
      en: 'Shakhrisabz (station)',
      ru: 'Шахрисабз (станция)',
      key: 14,
    },
    {
      en: 'Bukhara (station)',
      ru: 'Бухара (станция)',
      key: 15,
    },
    {
      en: 'Navoi (station)',
      ru: 'Навои (станция)',
      key: 16,
    },
    {
      en: 'Kongirot (station)',
      ru: 'Конгирот (станция)',
      key: 17,
    },
    {
      en: 'Nukus (station)',
      ru: 'Нукус (станция)',
      key: 18,
    },
    {
      en: 'Khiva (station)',
      ru: 'Хива (станция)',
      key: 19,
    },
    {
      en: 'Urgench (station)',
      ru: 'Ургенч (станция)',
      key: 20,
    },
    {
      en: 'Shovot (station)',
      ru: 'Шовот (станция)',
      key: 21,
    },
    {
      en: 'Andijan (station)',
      ru: 'Андижан (станция)',
      key: 22,
    },
    {
      en: 'Margilan (station)',
      ru: 'Маргилан (станция)',
      key: 23,
    },
    {
      en: 'Kokand (station)',
      ru: 'Коканд (станция)',
      key: 24,
    },
    {
      en: 'Pop (station)',
      ru: 'Поп (станция)',
      key: 25,
    },
    {
      en: 'Termiz (station)',
      ru: 'Термиз (станция)',
      key: 26,
    },
    {
      en: 'Sariosiyo (station)',
      ru: 'Сариосие (станция)',
      key: 27,
    },

    {
      en: 'Ferghana (station)',
      ru: 'Фергана (станция)',
      key: 28,
    },
    {
      en: 'Jizzakh (station)',
      ru: 'Жиззах (станция)',
      key: 29,
    },
    {
      en: 'Tortkol',
      ru: 'Торткол (станция)',
      key: 30,
    },
    {
      en: 'Miskin (station)',
      ru: 'Мискин (станция)',
      key: 31,
    },
    {
      en: 'Darband (station)',
      ru: 'Дарбанд (станция)',
      key: 32,
    },
    {
      en: 'Kumkurgan (station)',
      ru: 'Кумкурган (станция)',
      key: 33,
    },
    {
      en: 'Gulistan (station)',
      ru: 'Гулистан (станция)',
      key: 34,
    },
    {
      en: 'Ablyk (station)',
      ru: 'Аблик (станция)',
      key: 35,
    },
    {
      en: 'Akaltyn (station)',
      ru: 'Акалтин (станция)',
      key: 36,
    },
    {
      en: 'Akkavak (station)',
      ru: 'Аккавак (станция)',
      key: 37,
    },
    {
      ru: 'Акча (станция)',
      en: 'Akcha (station)',
      key: 38,
    },
    {
      en: 'Almazar (station)',
      ru: 'Алмазар (станция)',
      key: 39,
    },
    {
      en: 'Angren (station)',
      ru: 'Ангрен (станция)',
      key: 40,
    },
    {
      en: 'Aranchi (station)',
      ru: 'Аранчи (станция)',
      key: 41,
    },
    {
      en: 'Akhangaran (station)',
      ru: 'Ахангаран (станция)',
      key: 42,
    },
    {
      en: 'Barrage (station)',
      ru: 'Барраге (станция)',
      key: 43,
    },
    {
      en: 'Bakht (station)',
      ru: 'Бахт (станция)',
      key: 44,
    },
    {
      en: 'Bayaut (station)',
      ru: 'Баяут (станция)',
      key: 45,
    },
    {
      en: 'Bekabad (station)',
      ru: 'Бекабад (станция)',
      key: 46,
    },
    {
      en: 'Bozsu (station)',
      ru: 'Бозсу (станция)',
      key: 47,
    },
    {
      en: 'Boytog (station)',
      ru: 'Бойтог (станция)',
      key: 48,
    },
    {
      en: 'Gulistan (station)',
      ru: 'Гулистан (станция)',
      key: 49,
    },
    {
      en: 'Daliguzur (station)',
      ru: 'Далигузур (станция)',
      key: 50,
    },
    {
      en: 'Dashtobod (station)',
      ru: 'Даштобод (станция)',
      key: 51,
    },
    {
      en: 'Dashtobod (station)',
      ru: 'Даштобод (станция)',
      key: 52,
    },
    {
      en: 'Jizzakh (station)',
      ru: 'Жиззах (станция)',
      key: 53,
    },
    {
      en: 'Dustlik (station)',
      ru: 'Дустлик (станция)',
      key: 54,
    },
    {
      en: 'Zhaloir (station)',
      ru: 'Жалойр (станция)',
      key: 55,
    },
    {
      en: 'Zarbdar (station)',
      ru: 'Зарбдар (станция)',
      key: 56,
    },
    {
      en: 'Irdzharskaya (station)',
      ru: 'Иржарская (станция)',
      key: 57,
    },
    {
      en: 'Kadryra (station)',
      ru: 'Кадрийра (станция)',
      key: 58,
    },
    {
      en: 'Keles (station)',
      ru: 'Келес (станция)',
      key: 59,
    },
    {
      en: 'Kuchluk (station)',
      ru: 'Кучлук (станция)',
      key: 60,
    },
    {
      en: 'Nazarbek (station)',
      ru: 'Назарбек (станция)',
      key: 61,
    },
    {
      en: 'New Chinaz (station)',
      ru: 'Новый Чиназ (станция)',
      key: 62,
    },
    {
      en: 'Ozodlik (station)',
      ru: 'Озодлик (станция)',
      key: 63,
    },
    {
      en: 'Pakhta (station)',
      ru: 'Пахта (станция)',
      key: 64,
    },
    {
      en: 'Rakhimova (station)',
      ru: 'Рахимова (станция)',
      key: 65,
    },
    {
      en: 'Salar (station)',
      ru: 'Салар (станция)',
      key: 66,
    },
    {
      en: 'Sergeli (station)',
      ru: 'Сергели (станция)',
      key: 67,
    },
    {
      en: 'Syr-Darya (station)',
      ru: 'Сирдарья (станция)',
      key: 68,
    },
    {
      en: 'Toi-tepa (station)',
      ru: 'Тойтепа (станция)',
      key: 69,
    },
    {
      en: 'Tukimachi (station)',
      ru: 'Тукимачи (станция)',
      key: 70,
    },
    {
      en: 'Uzbekistan (station)',
      ru: 'Узбекистан (станция)',
      key: 71,
    },
    {
      en: 'Urta-aul (station)',
      ru: 'Урта-аул (станция)',
      key: 72,
    },
    {
      en: 'Farkhad (station)',
      ru: 'Фархад (станция)',
      key: 73,
    },
    {
      en: 'Havast (station)',
      ru: 'Хаваст (станция)',
      key: 74,
    },
    {
      en: 'Hamza (station)',
      ru: 'Xamza (станция)',
      key: 75,
    },
    {
      en: 'Hodzhikent (station)',
      ru: 'Ходжикент (станция)',
      key: 76,
    },
    {
      en: 'Chimkurgan (station)',
      ru: 'Чимкурган (станция)',
      key: 77,
    },
    {
      en: 'Chinaz (station)',
      ru: 'Чиназ (станция)',
      key: 78,
    },
    {
      en: 'Chirchik (station)',
      ru: 'Чирчик (станция)',
      key: 79,
    },
    {
      en: 'Chukursoy (station)',
      ru: 'Чукурсой (станция)',
      key: 80,
    },
    {
      en: 'Yalangach (station)',
      ru: 'Ялангач (станция)',
      key: 81,
    },
    {
      en: 'Yangiyer (station)',
      ru: 'Янгиер (станция)',
      key: 82,
    },
    {
      en: 'Yangiyul (station)',
      ru: 'Янгиюл (станция)',
      key: 83,
    },
    {
      en: 'Akbarabad (station)',
      ru: 'Акбарабад (станция)',
      key: 84,
    },
    {
      en: 'Altyaryk (station)',
      ru: 'Алтыярик (станция)',
      key: 85,
    },
    {
      en: 'Andijan-1 (station)',
      ru: 'Андижан-1 (станция)',
      key: 86,
    },
    {
      en: 'Andijan-2 (station)',
      ru: 'Андижань2 (станция)',
      key: 87,
    },
    {
      en: 'Arykbasha (station)',
      ru: 'Арикбаша (станция)',
      key: 88,
    },
    {
      en: 'Usak (station)',
      ru: 'Усак (станция)',
      key: 89,
    },
    {
      en: 'Akhunbabayev (station)',
      ru: 'Аxунбабаев (станция)',
      key: 90,
    },
    {
      en: 'Buvayda (station)',
      ru: 'Бувайда (станция)',
      key: 91,
    },
    {
      en: 'Grunchmazar (station)',
      ru: 'Грунч-мазар (станция)',
      key: 92,
    },
    {
      en: 'Kakir (station)',
      ru: 'Какир (станция)',
      key: 93,
    },
    {
      en: 'Kirgili (station)',
      ru: 'Киргили (станция)',
      key: 94,
    },
    {
      en: 'Kokand (station)',
      ru: 'Коканд (станция)',
      key: 95,
    },
    {
      en: 'Kuva (station)',
      ru: 'Кува (станция)',
      key: 96,
    },
    {
      en: 'Kuwasai (station)',
      ru: 'Кувасай (станция)',
      key: 97,
    },
    {
      en: 'Margilan (station)',
      ru: 'Маргилан (станция)',
      key: 98,
    },
    {
      en: 'Mikhnatabad (station)',
      ru: 'Михнатабад (станция)',
      key: 99,
    },
    {
      en: 'Namangan (station)',
      ru: 'Наманган (станция)',
      key: 100,
    },
    {
      en: 'Paytug (station)',
      ru: 'Пайтуг (станция)',
      key: 101,
    },
    {
      en: 'Pap (station)',
      ru: 'Пап (станция)',
      key: 102,
    },
    {
      en: 'Rapkan (station)',
      ru: 'Рапкан (станция)',
      key: 103,
    },
    {
      en: 'Savay (station)',
      ru: 'Савай (станция)',
      key: 104,
    },
    {
      en: 'Suvonobod (station)',
      ru: 'Сувонобод (станция)',
      key: 105,
    },
    {
      en: 'Temiryulobod (station)',
      ru: 'Темирюлобод (станция)',
      key: 106,
    },
    {
      en: 'Tentyaksay (station)',
      ru: 'Тентяксай (станция)',
      key: 107,
    },
    {
      en: 'Turakurgan (station)',
      ru: 'Туракурган (станция)',
      key: 108,
    },
    {
      en: 'Uychi (station)',
      ru: 'Ujhi (станция)',
      key: 109,
    },
    {
      en: 'Uchkurgan (station)',
      ru: 'Учкурган (станция)',
      key: 110,
    },
    {
      en: 'Fergana-2 (station)',
      ru: 'Фергана-2 (станция)',
      key: 111,
    },
    {
      en: 'Furkat (station)',
      ru: 'Фуркат (станция)',
      key: 112,
    },
  ])

  const handleChange = (index) => (ev) => {
    if (index >= 0) {
      let addresses = data.addresses
      addresses[index] = {
        ...addresses[index],
        [ev.target.name]: ev.target.value,
      }
      setData({
        ...data,
        addresses: addresses,
      })
    } else setData({ ...data, [ev.target.name]: ev.target.value })
  }

  const handleTourType = (type) => (e) => {
    if (type === 0) setData({ ...data, tour_type: type })
    setTourType(type)
  }

  const handleChoose = (item) => (e) => {
    setData({ ...data, tour_type: item })
    console.log(type)
  }

  const [dialog, setDialog] = useState(false)

  const handleIncrement = (index) => (e) => {
    let addresses = data.addresses
    addresses.push(address)
    setData({ ...data, addresses: addresses })
  }

  const handleDecrement = (index) => (e) => {
    let addresses = data.addresses
    addresses.splice(index, 1)
    setData({ ...data, addresses: addresses })
  }

  const handleSubmit = async (e) => {
    setDialog(true)
    try {
      const response = await axios.post('/api/bot', {
        ...data,
      })

      if (response.status === 200) setDialog(true)
    } catch (err) {
      console.log(err)
      // alert('Somethin went wrong, try again')
    }
  }

  const handleDatePick = (ind, start_date, end_date) => {
    if (ind >= 0) {
      let addresses = data.addresses
      addresses[ind] = {
        ...addresses[ind],
        start_date: start_date ? start_date.toDateString() : '',
        end_date: end_date ? end_date.toDateString() : '',
      }
      setData({
        ...data,
        addresses: addresses,
      })
    }
  }

  return (
    <section className={cls.container}>
      {dialog ? <Success setDialog={setDialog} /> : ''}
      <div className={cls.wrapper}>
        <Container>
          <Grid container>
            <Grid lg={12} xs={12} md={12}>
              <div className={cls.form}>
                <div className={cls.inputs}>
                  <div className={`${cls.form_group} ${cls.form_select}`}>
                    <Select
                      lists={tours}
                      selected={data.tour_type}
                      handleChoose={handleChoose}
                      data={data}
                      setData={setData}
                      type={`tour_type`}
                    />
                    <Select
                      lists={car_classes}
                      selected={data.car_class}
                      handleChoose={handleChoose}
                      data={data}
                      setData={setData}
                      type={'car_class'}
                    />
                  </div>
                  {data.addresses.map((item, i) => (
                    <div className={cls.form_group} key={i}>
                      {data.tour_type.en === 'Multi City Trip' ? (
                        i === 0 ? (
                          <span
                            className={`${cls.action_button_plus} ${cls.action_button}`}
                            onClick={handleIncrement(i)}
                          >
                            <LocalHospitalOutlinedIcon />
                          </span>
                        ) : (
                          <span
                            className={`${cls.action_button_minus} ${cls.action_button}`}
                            onClick={handleDecrement(i)}
                          >
                            <IndeterminateCheckBoxOutlinedIcon />
                          </span>
                        )
                      ) : (
                        ''
                      )}
                      <Input
                        htmlFor={'from_place'}
                        name={`from_place`}
                        id={`from_place`}
                        placeholder={i18n.language === 'en' ? `From` : 'Откуда'}
                        value={item.from_place}
                        onChange={handleChange(i)}
                        regions={regions}
                        index={i}
                        setData={setData}
                        data={data}
                      >
                        <PlaceOutlinedIcon />
                      </Input>

                      <Input
                        htmlFor={'to_place'}
                        name={`to_place`}
                        id={`to_place`}
                        placeholder={i18n.language === 'en' ? `To` : 'Куда'}
                        value={item.to_place}
                        onChange={handleChange(i)}
                        regions={regions}
                        index={i}
                        setData={setData}
                        data={data}
                      >
                        <GpsFixedOutlinedIcon />
                      </Input>

                      <Input
                        htmlFor={'start_date'}
                        name={`start_date`}
                        id={`start_date`}
                        placeholder={
                          i18n.language === 'en'
                            ? `Arrival Date`
                            : 'Время начало'
                        }
                        startDate={item.start_date}
                        endDate={item.end_date}
                        onChange={handleDatePick}
                        type={`date`}
                        isDate={true}
                        index={i}
                      ></Input>
                      {/* 
                      <Input
                        htmlFor={'end_date'}
                        name={`end_date`}
                        id={`end_date`}
                        placeholder={
                          i18n.language === 'en'
                            ? `Last Date`
                            : 'Время прибывания'
                        }
                        value={item.end_date}
                        onChange={handleChange(i)}
                        type={`date`}
                        isDate={true}
                      ></Input> */}
                    </div>
                  ))}
                  <div className={cls.form_group}>
                    <Input
                      htmlFor={'passengers'}
                      name={`passengers`}
                      id={`passengers`}
                      placeholder={
                        i18n.language === 'en' ? `Passengers` : 'Пассажиры'
                      }
                      value={data.passengers}
                      onChange={handleChange(-1)}
                      type={`number`}
                      isDate={true}
                    >
                      <SupervisorAccountOutlinedIcon />
                    </Input>
                    <Input
                      htmlFor={'email'}
                      name={`email`}
                      id={`email`}
                      placeholder={`Email`}
                      value={data.email}
                      onChange={handleChange(-1)}
                      type={`mail`}
                      isDate={true}
                    >
                      <MailOutlineOutlinedIcon />
                    </Input>
                    <Input
                      htmlFor={'comment'}
                      name={`comment`}
                      id={`comments`}
                      placeholder={
                        i18n.language === 'en' ? `Comments` : 'Комментария'
                      }
                      value={data.comment}
                      onChange={handleChange(-1)}
                      isDate={true}
                    >
                      <MessageOutlinedIcon />
                    </Input>
                    <div className={`${cls.label} ${cls.button}`}>
                      <Button
                        fullWidth
                        color={`secondary`}
                        onClick={handleSubmit}
                      >
                        {i18n.language === 'en' ? 'Request Quote' : 'Запрос'}
                      </Button>
                    </div>
                  </div>
                  {/* <div className={cls.form_group}>
                    {categories.map((item, i) => (
                      <div
                        key={i}
                        className={`${
                          selectedCarClass[i18n.language] ===
                          item[i18n.language]
                            ? cls.car_option_active
                            : ''
                        } ${cls.car_option}`}
                        onClick={() => {
                          setSelectedCarClass(item)
                          setData({ ...data, car_class: item })
                        }}
                      >
                        {item[i18n.language]}
                      </div>
                    ))}
                  </div> */}
                </div>
                {/* <div className={cls.actions}>
                  <p>
                    {i18n.language === 'en'
                      ? 'Prices Start from'
                      : 'Начинается от '}
                    <h1>{selectedCarClass.price} USD</h1>
                  </p>
                  <Button fullWidth color={`secondary`} onClick={handleSubmit}>
                    {i18n.language === 'en' ? 'Get A Quote' : 'Запрос'}
                  </Button>
                </div> */}
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
    </section>
  )
}

export default Hero
