import React from 'react'
import cls from './success.module.scss'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'
import HighlightOffOutlinedIcon from '@material-ui/icons/HighlightOffOutlined'
import { i18n } from '../../i18n'

const Success = (props) => {
  const { setDialog } = props
  return (
    <div className={cls.container}>
      <CheckCircleOutlineOutlinedIcon />
      <h2>{i18n.language === 'en' ? 'Successfull' : 'Успешно'}</h2>
      <span
        className={cls.close}
        onClick={() => {
          setDialog(false)
        }}
      >
        <HighlightOffOutlinedIcon />
      </span>
    </div>
  )
}

export default Success
