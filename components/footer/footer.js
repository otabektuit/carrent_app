import React from 'react'
import cls from './footer.module.scss'

const Footer = () => {
  return (
    <footer className={cls.container}>
      <img src={`/images/glt1.png`} alt={`GLT image`} width={200} />
      <p>Copyright 2020 RentCars. All rights deserved</p>
      <a href={'https://otabektuit.gitlab.io/obdev/'}>Developed by Obdev</a>
    </footer>
  )
}

export default Footer
