import Link from 'next/link'
import React from 'react'
import cls from './CallUs.module.scss'
import PhoneRoundedIcon from '@material-ui/icons/PhoneRounded'

const CallUs = () => {
  return (
    <div className={cls.container}>
      <Link href={`tel:+998946264346`}>
        <PhoneRoundedIcon color={`secondary`} />
      </Link>
    </div>
  )
}

export default CallUs
